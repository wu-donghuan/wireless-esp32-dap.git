![背面](README_CN.assets/背面.png)

## 简介

只需要**合宙9.9的ESP32C3简约版**即可开始无线调试！通过USBIP协议栈和CMSIS-DAP协议栈实现。

> 👉在5米范围内，擦除并烧写100kb大小的固件(Hex固件) ：

<p align="center"><img src="https://user-images.githubusercontent.com/17078589/120925694-4bca0d80-c70c-11eb-91b7-ffa54770faea.gif"/></p>

----

对于Keil用户，我们现在支持[elaphureLink](https://github.com/windowsair/elaphureLink)。无需usbip即可开始您的无线调试之旅！

![1679409793691](README_CN.assets/1679409793691.jpg)

## 特性

1. 支持的ESP芯片
    - [x] ESP32C3

2. 支持的调试接口：
    - [x] SWD
    - [x] JTAG

3. 支持的USB通信协议：
    - [x] USB-HID
    - [x] WCID & WinUSB (默认)
4. 支持的调试跟踪器：
    - [x] TCP转发的串口

5. 其它
    - [x] 通过SPI接口加速的SWD协议（最高可达40MHz）
    - [x] 支持[elaphureLink](https://github.com/windowsair/elaphureLink)，无需驱动的快速Keil调试
    - [x] 整个工程成本不超过12块RMB就能实现。

## 连接你的开发板

### WIFI连接

固件默认的WIFI SSID是`DAP`或者`OTA`，密码是`12345678`。

你可以在[wifi_configuration.h](main/wifi_configuration.h)文件中添加多个无线接入点。

你还可以在上面的配置文件中修改IP地址（但是我们更推荐你通过在路由器上绑定静态IP地址）。

![image-20230321224009975](README_CN.assets/image-20230321224009975.png)

固件中已经内置了一个mDNS服务。你可以通过`dap.local`的地址访问到设备。


### 调试接口连接


<details>
<summary>ESP32C3</summary>

| SWD            |        |
|----------------|--------|
| SWCLK          | GPIO6  |
| SWDIO          | GPIO7  |
| TVCC           | 3V3    |
| GND            | GND    |


--------------


| JTAG               |         |
|--------------------|---------|
| TCK                | GPIO6   |
| TMS                | GPIO7   |
| TDI                | GPIO9   |
| TDO                | GPIO8   |
| nTRST \(optional\) | GPIO4   |
| nRESET             | GPIO5   |
| TVCC               | 3V3     |
| GND                | GND     |

--------------

| Other             |        |
| ----------------- | ------ |
| LED\_WIFI\_STATUS | GPIO12 |
| Tx                | GPIO19 |
| Rx                | GPIO18 |


> Rx和Tx用于TCP转发的串口，默认不开启该功能。


</details>


----

## 硬件参考电路

硬件使用合宙9.9的ESP32-C3，通过制作一个底板的方式将引脚引出，详细见文件夹：[1.Hardware](1.Hardware)

![image-20230321225154149](README_CN.assets/image-20230321225154149.png)

你也可以去以下开源平台查看工程：https://oshwhub.com/badboyhuan/wu-xian-daplink-di-zuo_copy

------

## 编译固件并烧写

### 在本地构建并烧写


<details>
<summary>ESP32/ESP32C3</summary>

1. 获取esp-idf

    目前，请考虑使用esp-idf v4.4.2： https://github.com/espressif/esp-idf/releases/tag/v4.4.2

2. 编译和烧写

    使用ESP-IDF编译系统进行构建。
    更多的信息，请见：[Build System](https://docs.espressif.com/projects/esp-idf/en/latest/api-guides/build-system.html "Build System")


下面例子展示了在Windows上完成这些任务的一种可行方法：

```bash
# 编译
idf.py build
# 烧写
idf.py -p /dev/ttyS5 flash
```


> 位于项目根目录的`idf.py`脚本仅适用于较老的ESP8266设备，请不要在ESP32设备上使用。

</details>


> 我们还提供了预编译固件用于快速评估。详见 [wireless-esp32-dap 发行版 - Gitee.com](https://gitee.com/wu-donghuan/wireless-esp32-dap/releases)


## 使用

1. 获取USBIP项目

- Windows: [usbip-win](https://github.com/cezanne/usbip-win/tags)。

2. 获得的.ZIP文件解压，放在一个文件夹里，并在系统的环境变量里添加位置。

3. 安装 USB/IP 测试证书：

   - 安装（密码：usbip）`driver/usbip_test.pfx`
   - 证书应安装在
     1. “本地计算机”中的“受信任的根证书颁发机构”（不是当前用户）*和*
     2. “本地计算机”中的“受信任的发布者”（不是当前用户）

4. 在命令行输入：（开启系统测试模式）

   ```
   bcdedit.exe /set TESTSIGNING ON
   ```

5. 重新启动系统以应用
   重启后会出现下图：
   ![image-20230321223239477](README_CN.assets/image-20230321223239477.png)

6. 在管理员身份下运行CMD输入如下命令：

   ```
   usbip.exe install
   ```

7. 启动ESP32-C3并且把ESP32-C3连接到同一个WIFI下。

8. 通过USBIP连接ESP32：

```bash
# 👉 推荐HID模式或者WinUSB模式。用于usbip-win 0.3.6 版本。
usbip.exe attach -r <usbip server ip> -b 1-1
```

如果一切顺利，你应该看到你的设备被连接，如下图所示。

![image-20230321222750084](README_CN.assets/image-20230321222750084.png)

下面我们用keil MDK来测试：

![image-20230321222936834](README_CN.assets/image-20230321222936834.png)

------

## 经常会问的问题

### Keil提示“RDDI-DAP ERROR”或“SWD/JTAG Communication Failure”

1. 检查线路连接。别忘了连接3V3引脚。
2. 检查网络连接是否稳定。


## DAP很慢或者不稳定

注意，本项目受限于周围的网络环境。如果你在电脑上使用热点进行连接，你可以尝试使用wireshark等工具对网络连接进行分析。当调试闲置时，线路上应保持静默，而正常工作时一般不会发生太多的丢包。

一些局域网广播数据包可能会造成严重影响，这些包可能由这些应用发出：
- DropBox LAN Sync
- Logitech Arx Control
- ...

对于ESP32, 这无异于UDP洪水攻击...😰


周围的射频环境同样会造成影响，此外距离、网卡性能等也可能是需要考虑的。

## 文档

### 速度策略

单独使用ESP32通用IO时的最大翻转速率只有大概2MHz。当你选择最大时钟时，我们需要采取以下操作：

- `clock < 2Mhz` ：与你选择的时钟速度类似。
- `2MHz <= clock < 10MHz` ：使用最快的纯IO速度。
- `clock >= 10MHz` ：使用40MHz时钟的SPI加速。

> 请注意，这个项目最重要的速度制约因素仍然是TCP连接速度。

### 对于OpenOCD用户

这个项目最初是为在Keil上运行而设计的，但现在你也可以在OpenOCD上通过它来烧录程序。
注意，如果你想使用40MHz的SPI加速器，你需要在连接目标设备后指定速度，否则会在开始时失败。

```bash
# 在使用flash指令前需要先运行：
> adapter speed 10000

> halt
> flash write_image [erase] [unlock] filename [offset] [type]
```

> Keil的操作时序与OpenOCD的有些不同。例如，OpenOCD在读取 "IDCODE "寄存器之前缺少SWD线复位序列。

### 系统 OTA

当这个项目被更新时，你可以通过无线方式更新固件。

请访问以下网站了解OTA操作。[在线OTA](http://corsacota.surge.sh/?address=dap.local:3241)

对于大多数ESP32设备，你不需要关心闪存的大小。然而，闪存大小设置不当可能会导致OTA失败。在这种情况下，请用`idf.py menuconfig`改变闪存大小，或者修改`sdkconfig`：

```
# 选择一个flash大小
CONFIG_ESPTOOLPY_FLASHSIZE_1MB=y
CONFIG_ESPTOOLPY_FLASHSIZE_2MB=y
CONFIG_ESPTOOLPY_FLASHSIZE_4MB=y
CONFIG_ESPTOOLPY_FLASHSIZE_8MB=y
CONFIG_ESPTOOLPY_FLASHSIZE_16MB=y

# 然后设置flash大小
CONFIG_ESPTOOLPY_FLASHSIZE="2MB"
```

如果闪存大小为2MB，sdkconfig文件会看起来像这样：

```
CONFIG_ESPTOOLPY_FLASHSIZE_2MB=y
CONFIG_ESPTOOLPY_FLASHSIZE="2MB"
```

可以用esptool.py工具检查你使用的ESP设备闪存大小：

```bash
esptool.py -p (PORT) flash_id
```

### TCP转发的串口

该功能在TCP和Uart之间提供了一个桥梁：
```
发送数据   ->  TCP  ->  Uart TX -> 外部设备

接收数据   <-  TCP  <-  Uart Rx <- 外部设备
```

当TCP连接建立后，ESP芯片将尝试解决首次发送的文本。当文本是一个有效的波特率时，转发器就会切换到该波特率。例如，发送ASCII文本`115200`会将波特率切换为115200。
由于性能原因，该功能默认不启用。你可以修改 [wifi_configuration.h](main/wifi_configuration.h) 来打开它。

![abc](README_CN.assets/abc.gif)

----

## 开发

请查看其他分支以了解最新的开发进展。我们欢迎任何形式的贡献，包括但不限于新功能、关于电路的想法和文档。

如果你有什么想法，欢迎在下面提出：
- [Issues · wu-donghuan/wireless-esp32-dap - Gitee.com](https://gitee.com/wu-donghuan/wireless-esp32-dap/issues)
- [Pull Requests · wu-donghuan/wireless-esp32-dap - Gitee.com](https://gitee.com/wu-donghuan/wireless-esp32-dap/pulls)


# 致谢

归功于以下项目、人员和组织。

> - https://github.com/thevoidnn/esp8266-wifi-cmsis-dap for adapter firmware based on CMSIS-DAP v1.0
> - https://github.com/ARM-software/CMSIS_5 for CMSIS
> - https://github.com/cezanne/usbip-win for usbip windows
> - [9.9元自制WIFI无线DAPLINK调试器 – 晨旭的博客~ (chenxublog.com)](https://www.chenxublog.com/2022/10/24/9-9-wifi-daplink-by-hand.html)for 9.9daplink
> - [windowsair/wireless-esp8266-dap: CMSIS-DAP compatible wireless debugger for various ESP chips such as ESP8266, ESP32. Optional 40MHz SPI acceleration, etc. 适配多种ESP芯片的无线调试器 (github.com)](https://github.com/windowsair/wireless-esp8266-dap)


- [@HeavenSpree](https://www.github.com/HeavenSpree)
- [@Zy19930907](https://www.github.com/Zy19930907)
- [@caiguang1997](https://www.github.com/caiguang1997)
- [@ZhuYanzhen1](https://www.github.com/ZhuYanzhen1)


## 许可证
[MIT 许可证](LICENSE)
